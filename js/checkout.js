$(document).ready(function() {

    var payment_active = false;

    function init(){

      var items_json = '{'+
                    '"items": [{'+
                        '"id": "1",'+
                        '"name": "ASUS ZenPad Z300M-A2-GR",'+
                        '"desc": "16GB Negro, Gris - Tablet (Tableta de tamaño completo, IEEE 802.11n, Android, Pizarra, Android 6.0, Negro, Gris)",'+
                        '"price": "3380.95",'+
                        '"image": "images/asus.png",'+
                        '"quantity": "2"'+
                      '},'+
                      '{'+
                        '"id": "2",'+
                        '"name": "Lenovo TAB3 Essential - ZA0R0029US",'+
                        '"desc": "7 Android Tablet",'+
                        '"price": "1495.31",'+
                        '"image": "images/lenovo.png",'+
                        '"quantity": "1"'+
                      '},'+
                      '{'+
                        '"id": "3",'+
                        '"name": "Star Wars Figuras de Accion Star Wars Black Series",'+
                        '"desc": "AT-AT Driver, 6",'+
                        '"price": "449",'+
                        '"image": "images/starwars.png",'+
                        '"quantity": "1"'+
                      '}'+
                    ']'+
                   '}';

      var obj = JSON.parse(items_json);
      var quantity;
      var price;
      var options;
      var subtotal_item;

      for (var i = obj.items.length - 1; i >= 0; i--) {

        quantity = obj.items[i].quantity;
        price = obj.items[i].price;
        subtotal_item = quantity*price;
        options = "";

        for (var j = 1; j < 6; j++) {
          if ( j == quantity) {
            options += '<option selected value="'+j+'">'+j+'</option>';
          }else{ 
            options += '<option value="'+j+'">'+j+'</option>';
          }
        }
        // add items to the car list
        $('<div id="item'+obj.items[i].id+'" class="car-item"><div class="flex-grid"><div class="car-item-image responsive"><img id="item-image" src="'+obj.items[i].image+'"></div><div class="car-item-data"><div class="item-name"><span id="item-name">'+obj.items[i].name+'</span></div><div class="item-desc"><span id="item-desc">'+obj.items[i].desc+'</span></div><div class="item-quantity"><label>Cantidad </label><select class="item-quantity" id="item-quantity-'+obj.items[i].id+'" name="item-quantity" form="checkout-form">'+options+'</select></div><div class="item-price"><label>Precio Unitario </label><span class="item-price" id="item-price-'+obj.items[i].id+'">$'+obj.items[i].price+'</span></div><div class="item-subtotal"><label>Subtotal </label><span class="item-subtotal" id="item-subtotal-'+obj.items[i].id+'">$'+subtotal_item+'</span></div></div></div><div class="erase"><a class="erase-div" href="#item'+obj.items[i].id+'"><i class="fas fa-times fa-1x"></i></a><label>Quitar<label></div></div>').appendTo('#car-list');
      }

      //Set Payment secion enable until Shipping data is fullfiled
      $('#payment-data').removeClass('active');
      $('.accordion ' + '#payment-data').slideUp(300).removeClass('open');

      car_items();
      subtotal_item_price();
      subtotal();
      total();

    }

    function activate_payment(){
      //Set Payment secion active
      $('#payment-data').addClass('active');
      $('.accordion ' + '#payment-data').slideDown(300).addClass('open');
    }

    // Subtotal item
    function subtotal_item_price(){

      var id
          ,price_item
          ,quantity_item
          ,price
          ,quantity
          ,subtotal_item
          ,subtotal=0;

      $('.car-item').each(function(index){

          id = index+1;
          price_item = 'item-price-'+id;
          quantity_item = 'item-quantity-'+id;
          subtotal_item = 'item-subtotal-'+id;
          price = $('#'+price_item).text().replace('$','');
          quantity = $('#'+quantity_item).val();
          subtotal = parseFloat(price)*parseFloat(quantity);
          $('#'+subtotal_item).text('$'+subtotal);
      });          
    }

    // Subtotal
    function subtotal(){

      var sub_item
          ,id
          ,sub_total_item
          ,subtotal=0;

      $('.car-item').each(function(index){
          id = index+1;
          sub_item = 'item-subtotal-'+id;
          sub_total_item = $('#'+sub_item).text().replace('$','');
          subtotal += parseFloat(sub_total_item);
      });

      $('#subtotal').text('$'+subtotal);
    }

    // Total
    function total(){

      var shipping_price = $('input[name=shipping-method]:checked', '#checkout-form').val();
      var subtotal = $('#subtotal').text().replace('$','');
      
      $('#shipping-pay').text('$'+shipping_price);

      var total = parseFloat(subtotal) + parseFloat(shipping_price);

      $('#total').text('$'+total);
    }

    // HAndler of update items total quantity
    function car_items(){

      var total_items=0
          ,quantity_item
          ,quantity
          ,id;

      $('.car-item').each(function(index){
        id = index+1;
        quantity_item = 'item-quantity-'+id;
        quantity = $('#'+quantity_item).val();
        total_items += parseInt(quantity);
      });

      $('#car_quantity').text(total_items);
      $('#car_quantity_h').text('('+total_items+')');
    }

    // Handler of the change of shipping-method
    $('input:radio[name=shipping-method]').change(function() {
      total();
    });

    // Handler of the change of quantity item
    $( ".item-quantity" ).change(function() {
      car_items();
      subtotal_item_price();
      subtotal();
      total();
    });

    //Erase div-item 
    $('.erase-div').click(function(e) {
      
      var currentDiv = $(this).attr('href');

      $(currentDiv).remove();
      car_items();
      subtotal();
      total();

      e.preventDefault();
    });

    //Accordion functionality
    $('.accordion-section-title').click(function(e) {
      
      var currentAttrValue = $(this).attr('href');

      if($(e.target).is('.active')) {
        $(this).removeClass('active');
        $('.accordion ' + currentAttrValue).slideUp(300).removeClass('open');
      } else {
        if ( currentAttrValue != '#payment-data' ){
          $(this).addClass('active');
          $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
        } else {
          if ( payment_active ){
            $(this).addClass('active');
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
          } 
        }
      }
      e.preventDefault();
    });

    //
    $('#addpayment').click(function(e) {
      
      payment_active = true;

      $('#shipping-data').removeClass('active');
      $('.accordion ' + '#shipping-data').slideUp(300).removeClass('open');

      $('#payment-data').addClass('active');
      $('.accordion ' + '#payment-data').slideDown(300).addClass('open');
      e.preventDefault();
    });

    init();
});